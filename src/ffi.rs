use crate::GridUnit;

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
#[repr(u8)]
pub enum GamepadButton {
    Up = 0,
    Down,
    Left,
    Right,
    Select,
    Start,
    LeftShoulder,
    RightShoulder,
    North,
    South,
    East,
    West,
}

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
#[repr(u8)]
pub enum OscillatorType {
    Sine = 0,
    Square,
    Sawtooth,
    Triangle,
}

#[derive(Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
#[repr(u8)]
pub enum CompositeOperation {
    /// This is the default setting and draws new shapes on top of the existing canvas content.
    SourceOver = 0,

    /// The new shape is drawn only where both the new shape and the destination canvas overlap.
    /// Everything else is made transparent.
    SourceIn,

    /// The new shape is drawn where it doesn't overlap the existing canvas content.
    SourceOut,

    /// The new shape is only drawn where it overlaps the existing canvas content.
    SourceAtop,

    /// New shapes are drawn behind the existing canvas content.
    DestinationOver,

    /// The existing canvas content is kept where both the new shape and existing canvas content
    /// overlap. Everything else is made transparent.
    DestinationIn,

    /// The existing content is kept where it doesn't overlap the new shape.
    DestinationOut,

    /// The existing canvas is only kept where it overlaps the new shape. The new shape is drawn
    /// behind the canvas content.
    DestinationAtop,

    /// Where both shapes overlap the color is determined by adding color values.
    Lighter,

    /// Only the new shape is shown.
    Copy,

    /// Shapes are made transparent where both overlap and drawn normal everywhere else.
    Xor,

    /// The pixels of the top layer are multiplied with the corresponding pixel of the bottom
    /// layer. A darker picture is the result.
    Multiply,

    /// The pixels are inverted, multiplied, and inverted again. A lighter picture is the result
    /// (opposite of multiply)
    Screen,

    /// A combination of multiply and screen. Dark parts on the base layer become darker, and light
    /// parts become lighter.
    Overlay,

    /// Retains the darkest pixels of both layers.
    Darken,

    /// Retains the lightest pixels of both layers.
    Lighten,

    /// Divides the bottom layer by the inverted top layer.
    ColorDodge,

    /// Divides the inverted bottom layer by the top layer, and then inverts the result.
    ColorBurn,

    /// A combination of multiply and screen like overlay, but with top and bottom layer swapped.
    HardLight,

    /// A softer version of hardLight. Pure black or white does not result in pure black or white.
    SoftLight,

    /// Subtracts the bottom layer from the top layer or the other way round to always get a
    /// positive value.
    Difference,

    /// Like difference, but with lower contrast.
    Exclusion,

    /// Preserves the luma and chroma of the bottom layer, while adopting the hue of the top layer.
    Hue,

    /// Preserves the luma and hue of the bottom layer, while adopting the chroma of the top layer.
    Saturation,

    /// Preserves the luma of the bottom layer, while adopting the hue and chroma of the top layer.
    Color,

    /// Preserves the hue and chroma of the bottom layer, while adopting the luma of the top layer.
    Luminosity,
}

extern "C" {
    pub fn qwac_log(string: *const u8, length: u32);

    pub fn qwac_canvas_create() -> u32;
    pub fn qwac_canvas_resize(canvas: u32, width_px: u32, height_px: u32);
    pub fn qwac_canvas_clear(canvas: u32);
    pub fn qwac_canvas_set_global_alpha(canvas: u32, alpha: f32);
    pub fn qwac_canvas_set_global_composite_operation(canvas: u32, operation: CompositeOperation);
    pub fn qwac_canvas_draw_rectangle(
        canvas: u32,
        x: GridUnit,
        y: GridUnit,
        width: GridUnit,
        height: GridUnit,
        r: u8,
        g: u8,
        b: u8,
        a: u8,
    );

    pub fn qwac_canvas_draw_texture(
        canvas: u32,
        texture: u32,
        source_x: GridUnit,
        source_y: GridUnit,
        source_width: GridUnit,
        source_height: GridUnit,
        dest_x: GridUnit,
        dest_y: GridUnit,
        dest_width: GridUnit,
        dest_height: GridUnit,
    );

    pub fn qwac_canvas_draw_canvas(
        canvas: u32,
        source_canvas: u32,
        source_x: GridUnit,
        source_y: GridUnit,
        source_width: GridUnit,
        source_height: GridUnit,
        dest_x: GridUnit,
        dest_y: GridUnit,
        dest_width: GridUnit,
        dest_height: GridUnit,
    );

    pub fn qwac_canvas_delete(canvas: u32);

    pub fn qwac_audio_buffer_from_opus(data: *const u8, size: u32) -> u32;
    pub fn qwac_audio_oscillator(oscillator_type: OscillatorType) -> u32;
    pub fn qwac_audio_source_delay(audio_node: u32, seconds: f32);
    pub fn qwac_audio_source_length(audio_node: u32, seconds: f32);
    pub fn qwac_audio_source_play(audio_node: u32);
    pub fn qwac_audio_source_stop(audio_node: u32);
    pub fn qwac_audio_gain() -> u32;
    pub fn qwac_audio_delay(max_seconds: f32) -> u32;
    pub fn qwac_audio_set_value(audio_node: u32, value: f32);
    pub fn qwac_audio_set_value_at_time(audio_node: u32, time: f32, value: f32);
    pub fn qwac_audio_linear_ramp_to_value_at_time(audio_node: u32, time: f32, value: f32);
    pub fn qwac_audio_exponential_ramp_to_value_at_time(audio_node: u32, time: f32, value: f32);
    pub fn qwac_audio_connect(source: u32, target: u32);
    pub fn qwac_audio_disconnect(source: u32, target: u32);
    pub fn qwac_audio_sink(audio_node: u32);
    pub fn qwac_audio_unsink(audio_node: u32);
    pub fn qwac_audio_delete(audio_node: u32);

    pub fn qwac_texture_from_canvas(canvas: u32) -> u32;
    pub fn qwac_texture_from_rgba(data: *const u8, width: u32, height: u32) -> u32;
    pub fn qwac_texture_from_png(data: *const u8, size: u32) -> u32;
    pub fn qwac_texture_from_webp(data: *const u8, size: u32) -> u32;
    pub fn qwac_texture_delete(texture: u32);

    pub fn qwac_data_save(data: *const u8, size: u32);
    pub fn qwac_data_load(data: *mut u8, size: u32) -> u32;
    pub fn qwac_data_size() -> u32;
}
