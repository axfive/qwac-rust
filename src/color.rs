#[derive(Default, Clone, Copy, Hash, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Rgba {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Rgba {
    /// Opaque white.
    pub fn white() -> Rgba {
        Rgba {
            r: u8::MAX,
            g: u8::MAX,
            b: u8::MAX,
            a: u8::MAX,
        }
    }

    /// Opaque black.
    pub fn black() -> Rgba {
        Rgba {
            r: u8::MIN,
            g: u8::MIN,
            b: u8::MIN,
            a: u8::MAX,
        }
    }

    /// Transparent black.
    pub fn clear() -> Rgba {
        Rgba {
            r: u8::MIN,
            g: u8::MIN,
            b: u8::MIN,
            a: u8::MIN,
        }
    }
}
