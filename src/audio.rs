use crate::ffi;

pub use crate::ffi::OscillatorType;

pub trait AudioNode {
    fn node_id(&self) -> u32;

    fn connect<A>(&self, target: &A)
    where
        A: AudioNode,
    {
        let id = self.node_id();
        let target = target.node_id();
        unsafe {
            ffi::qwac_audio_connect(id, target);
        }
    }

    fn disconnect<A>(&self, target: &A)
    where
        A: AudioNode,
    {
        let id = self.node_id();
        let target = target.node_id();
        unsafe {
            ffi::qwac_audio_disconnect(id, target);
        }
    }

    fn sink(&self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_sink(id);
        }
    }

    fn unsink(&self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_unsink(id);
        }
    }
}

pub trait AudioSource: AudioNode {
    fn play(&self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_source_play(id);
        }
    }

    fn stop(&self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_source_stop(id);
        }
    }

    fn delay(&mut self, seconds: f32) {
        unsafe {
            ffi::qwac_audio_source_delay(self.node_id(), seconds);
        }
    }

    fn length(&mut self, seconds: f32) {
        unsafe {
            ffi::qwac_audio_source_length(self.node_id(), seconds);
        }
    }
}

pub trait SingleValue: AudioNode {
    fn set_value(&mut self, value: f32) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_set_value(id, value);
        }
    }

    fn set_value_at_time(&mut self, time: f32, value: f32) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_set_value_at_time(id, time, value);
        }
    }

    fn linear_ramp_to_value_at_time(&mut self, time: f32, value: f32) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_linear_ramp_to_value_at_time(id, time, value);
        }
    }

    fn exponential_ramp_to_value_at_time(&mut self, time: f32, value: f32) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_exponential_ramp_to_value_at_time(id, time, value);
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Oscillator {
    id: u32,
}

impl AudioNode for Oscillator {
    fn node_id(&self) -> u32 {
        self.id
    }
}

impl SingleValue for Oscillator {}

impl AudioSource for Oscillator {}

impl Oscillator {
    pub fn new(oscillator_type: OscillatorType) -> Self {
        let id = unsafe { ffi::qwac_audio_oscillator(oscillator_type) };
        Self { id }
    }
}

impl Drop for Oscillator {
    fn drop(&mut self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_delete(id);
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AudioBuffer {
    id: u32,
}

impl AudioNode for AudioBuffer {
    fn node_id(&self) -> u32 {
        self.id
    }
}

impl SingleValue for AudioBuffer {}

impl AudioSource for AudioBuffer {}

impl AudioBuffer {
    pub fn from_opus<D: AsRef<[u8]>>(data: D) -> Self {
        let data = data.as_ref();
        let id = unsafe { ffi::qwac_audio_buffer_from_opus(data.as_ptr(), data.len() as u32) };
        Self { id }
    }
}

impl Drop for AudioBuffer {
    fn drop(&mut self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_delete(id);
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Gain {
    id: u32,
}

impl AudioNode for Gain {
    fn node_id(&self) -> u32 {
        self.id
    }
}

impl SingleValue for Gain {}

impl Gain {
    pub fn new() -> Self {
        let id = unsafe { ffi::qwac_audio_gain() };
        Self { id }
    }
}

impl Drop for Gain {
    fn drop(&mut self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_delete(id);
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Delay {
    id: u32,
}

impl AudioNode for Delay {
    fn node_id(&self) -> u32 {
        self.id
    }
}

impl SingleValue for Delay {}

impl Delay {
    pub fn new(max_seconds: f32) -> Self {
        let id = unsafe { ffi::qwac_audio_delay(max_seconds) };
        Self { id }
    }
}

impl Drop for Delay {
    fn drop(&mut self) {
        let id = self.node_id();
        unsafe {
            ffi::qwac_audio_delete(id);
        }
    }
}
