pub use crate::ffi::CompositeOperation;
use crate::texture::Texture;
use crate::{ffi, Rect, Rgba};
use core::sync::atomic::{AtomicBool, Ordering};

/// A draw destination trait, used for wrapping a canvas in something like a camera, or even a
/// split screen, to abstract drawing.
pub trait DrawDestination {
    fn draw_rectangle(&mut self, rectangle: Rect, color: Rgba);

    fn draw_texture(&mut self, texture: &Texture, source: Rect, destination: Rect);

    fn draw_canvas(&mut self, canvas: &Canvas, source: Rect, destination: Rect);
}

impl<D: DrawDestination + ?Sized> DrawDestination for &mut D {
    fn draw_rectangle(&mut self, rectangle: Rect, color: Rgba) {
        (**self).draw_rectangle(rectangle, color);
    }

    fn draw_texture(&mut self, texture: &Texture, source: Rect, destination: Rect) {
        (**self).draw_texture(texture, source, destination);
    }

    fn draw_canvas(&mut self, canvas: &Canvas, source: Rect, destination: Rect) {
        (**self).draw_canvas(canvas, source, destination);
    }
}

#[derive(Debug)]
pub struct Canvas {
    pub(crate) id: u32,
}

/// Whether or not there is currently a live screen.
static LIVE_SCREEN: AtomicBool = AtomicBool::new(false);

impl Canvas {
    pub fn new() -> Self {
        unsafe {
            Self {
                id: ffi::qwac_canvas_create(),
            }
        }
    }

    pub fn resize(&mut self, width_px: u32, height_px: u32) {
        unsafe {
            ffi::qwac_canvas_resize(self.id, width_px, height_px);
        }
    }

    pub fn clear(&mut self) {
        unsafe {
            ffi::qwac_canvas_clear(self.id);
        }
    }

    pub fn as_texture(&self) -> Texture {
        Texture::from(self)
    }

    pub fn set_global_alpha(&mut self, alpha: f32) {
        unsafe {
            ffi::qwac_canvas_set_global_alpha(self.id, alpha);
        }
    }

    pub fn set_global_composite_operation(&mut self, operation: CompositeOperation) {
        unsafe {
            ffi::qwac_canvas_set_global_composite_operation(self.id, operation);
        }
    }
}

impl DrawDestination for Canvas {
    fn draw_rectangle(&mut self, rectangle: Rect, color: Rgba) {
        unsafe {
            ffi::qwac_canvas_draw_rectangle(
                self.id,
                rectangle.origin.x,
                rectangle.origin.y,
                rectangle.size.width,
                rectangle.size.height,
                color.r,
                color.g,
                color.b,
                color.a,
            );
        }
    }

    fn draw_texture(&mut self, texture: &Texture, source: Rect, destination: Rect) {
        unsafe {
            ffi::qwac_canvas_draw_texture(
                self.id,
                texture.id,
                source.origin.x,
                source.origin.y,
                source.size.width,
                source.size.height,
                destination.origin.x,
                destination.origin.y,
                destination.size.width,
                destination.size.height,
            );
        }
    }

    fn draw_canvas(&mut self, canvas: &Canvas, source: Rect, destination: Rect) {
        unsafe {
            ffi::qwac_canvas_draw_canvas(
                self.id,
                canvas.id,
                source.origin.x,
                source.origin.y,
                source.size.width,
                source.size.height,
                destination.origin.x,
                destination.origin.y,
                destination.size.width,
                destination.size.height,
            );
        }
    }
}

impl Drop for Canvas {
    fn drop(&mut self) {
        if self.id != 0 {
            unsafe {
                ffi::qwac_canvas_delete(self.id);
            }
        }
    }
}

#[derive(Debug)]
pub struct Screen(Canvas);

impl core::ops::Deref for Screen {
    type Target = Canvas;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl core::ops::DerefMut for Screen {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Screen {
    /// Fetch the screen canvas.  Only one screen canvas may exist at a time, and cleanup performs
    /// no destruction or clearing.
    pub fn get() -> Self {
        if LIVE_SCREEN.swap(true, Ordering::Relaxed) {
            crate::log("ERROR: only one live screen may exist at once");
            panic!();
        }
        Self(Canvas { id: 0 })
    }
}

impl Drop for Screen {
    fn drop(&mut self) {
        LIVE_SCREEN.store(false, Ordering::Relaxed);
    }
}

impl DrawDestination for Screen {
    fn draw_rectangle(&mut self, rectangle: Rect, color: Rgba) {
        self.0.draw_rectangle(rectangle, color);
    }

    fn draw_texture(&mut self, texture: &Texture, source: Rect, destination: Rect) {
        self.0.draw_texture(texture, source, destination);
    }

    fn draw_canvas(&mut self, canvas: &Canvas, source: Rect, destination: Rect) {
        self.0.draw_canvas(canvas, source, destination);
    }
}
