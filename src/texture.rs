use crate::ffi;
use crate::Canvas;
use crate::Rgba;

#[derive(Debug)]
pub struct Texture {
    pub(crate) id: u32,
}

impl Texture {
    pub fn from_rgba<P: AsRef<[u8]>>(pixels: P, width: u32, height: u32) -> Self {
        let pixels = pixels.as_ref().as_ptr();
        let id = unsafe { ffi::qwac_texture_from_rgba(pixels, width, height) };
        Self { id }
    }

    pub fn from_png<D: AsRef<[u8]>>(data: D) -> Self {
        let data = data.as_ref();
        let id = unsafe { ffi::qwac_texture_from_png(data.as_ptr(), data.len() as u32) };
        Self { id }
    }

    pub fn from_webp<D: AsRef<[u8]>>(data: D) -> Self {
        let data = data.as_ref();
        let id = unsafe { ffi::qwac_texture_from_webp(data.as_ptr(), data.len() as u32) };
        Self { id }
    }

    /// Build the texture from individual pixels.  Much more efficient with std than without.
    /// Without, a temporary canvas is used and converted into the pixels in question.
    #[cfg(feature = "std")]
    pub fn from_pixels<P: Iterator<Item = Rgba>>(pixels: P, width: u32, height: u32) -> Self {
        let pixels: Vec<u8> = pixels
            .flat_map(|color| [color.r, color.g, color.b, color.a].into_iter())
            .collect();

        let id = unsafe { ffi::qwac_texture_from_rgba(pixels.as_ptr(), width, height) };
        Self { id }
    }

    /// Build the texture from individual pixels.  Much more efficient with std than without.
    /// Without, a temporary canvas is used and converted into the pixels in question.
    /// If not enough pixels are supplied, this panics.
    #[cfg(not(feature = "std"))]
    pub fn from_pixels<P: Iterator<Item = Rgba>>(mut pixels: P, width: u32, height: u32) -> Self {
        use crate::DrawDestination;
        use crate::GridUnit;

        let mut canvas = Canvas::new();
        canvas.resize(width, height);
        for y in 0..height {
            for x in 0..width {
                let pixel = pixels.next().unwrap();
                let position = crate::Rect::new(
                    crate::Point::new(x as GridUnit, y as GridUnit),
                    crate::Size::new(1 as GridUnit, 1 as GridUnit),
                );
                canvas.draw_rectangle(position, pixel);
            }
        }
        canvas.as_texture()
    }
}

impl<'a> From<&'a Canvas> for Texture {
    fn from(canvas: &'a Canvas) -> Self {
        let id = unsafe { ffi::qwac_texture_from_canvas(canvas.id) };
        Self { id }
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        if self.id != 0 {
            unsafe {
                ffi::qwac_texture_delete(self.id);
            }
        }
    }
}
